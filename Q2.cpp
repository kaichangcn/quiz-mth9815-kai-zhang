#include<iostream>
using namespace std;

class MaxHeap
{
private:
	int* heap;
    int curSize;
    int maxSize;
    
public:
    MaxHeap(int maxSize=10)
    {
	    this->maxSize = maxSize;
	    heap = new int[this->maxSize + 1];
	    curSize = 0;
	}
    int size() const
    {
    	return curSize;
	}
    int top() const
    {
    	if(curSize == 0)
    	{
    		cout << "empty\n";
		}
		return heap[1];
	}
    MaxHeap& Add(int n)
    {
	    if (curSize == maxSize)
	    {
	    	cout << "No more space\n";
		}
		curSize++;
	    int i = curSize;
	    while (i != 1 && n > heap[i / 2])
	    {
	        heap[i] = heap[i / 2];
	        i = i / 2;
	    }
	    heap[i] = n;
	    return *this;
	}
};

int main()
{
	MaxHeap heap(10);
	heap.Add(1);
	heap.Add(4);
	heap.Add(3);
	cout << heap.top() << "\n";
	return 0;
}

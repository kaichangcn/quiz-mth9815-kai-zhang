#include<iostream>
#include<vector>
using namespace std;

void print(vector<int>& v)
{
    for (auto it = v.begin(); it != v.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
}

void quickSort(vector<int>& v, int start, int last)
{
    int i = start;
    int j = last;
    int temp = v[i];
    if (i < j)
    {
        while (i < j)
        {
            while (i < j && v[j] >= temp)
            {
                j--;
            }
            if (i < j)
            {
                v[i] = v[j];
                i++;
            }

            while (i < j && temp > v[i])
            {
                i++;
            }
            if (i < j)
            {
                v[j] = v[i];
                j--;
            }

        }
        v[i] = temp;
        quickSort(v, start, i - 1);
        quickSort(v, i + 1, last);
    }
}

int main()
{
    vector<int> v = {10, 9, 4, 6, 7, 1, 2, 5, 8, 3};
    print(v);
    quickSort(v, 0, v.size() - 1);
    print(v);
    return 0;
}
